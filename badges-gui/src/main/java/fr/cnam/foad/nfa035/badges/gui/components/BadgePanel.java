package fr.cnam.foad.nfa035.badges.gui.components;

import fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BadgePanel extends JPanel {

    private DigitalBadge digitalBadge;

    private DirectAccessBadgeWalletDAO dao;

    /**
     * Creates a new <code>JPanel</code> with a double buffer
     * and a flow layout.
     */
    public BadgePanel(DigitalBadge digitalBadge, DirectAccessBadgeWalletDAO dao) {
        this.digitalBadge = digitalBadge;
        this.dao = dao;
    }

    public DigitalBadge getDigitalBadge() {
        return digitalBadge;
    }

    public void setDigitalBadge(DigitalBadge digitalBadge) {
        this.digitalBadge = digitalBadge;
    }

    public DirectAccessBadgeWalletDAO getDao() {
        return dao;
    }

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            dao.getBadgeFromMetadata(bos, digitalBadge);
            g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }



}
